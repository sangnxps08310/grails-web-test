dataSource {
    pooled = true
//    driverClassName = "com.mysql.jdbc.Driver"
    driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
    username = "sa"
    password = "123"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update"//""create-drop" // one of 'create', 'create-drop','update'
            url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=grails-test"
//            url = "jdbc:sqlserver://127.0.0.1/grails-test?useUnicode=yes&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:hsqldb:mem:testDb"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:hsqldb:file:prodDb;shutdown=true"
        }
    }
}
