package testgrails


class TestController {

    def index = {
        def student_temp = new Student();
        student_temp.name = "sang";
        student_temp.age = 12;
        student_temp.mark = 9.9;
        student_temp.save(flush: true)
        def list = Student.list();
        list?.each { student ->
            System.out.println(student.name)
        }
        [students: list]
    }
}
