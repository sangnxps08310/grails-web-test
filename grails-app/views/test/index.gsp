<%--
  Created by IntelliJ IDEA.
  User: nhm95
  Date: 1/7/2020
  Time: 11:36 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Test</title>
</head>

<body>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Mark</th>
        <th>Age</th>
    </tr>
    <g:each in="${students}" var="student">
        <tr>
            <td>${student.id}</td>
            <td>${student.name}</td>
            <td>${student.mark}</td>
            <td>${student.age}</td>
        </tr>
    </g:each>
</table>
</body>
</html>